class Timer
  def initialize(seconds = 0)
    @seconds = seconds
  end

  attr_accessor :seconds

  def time_string
    minutes = @seconds / 60
    hours = minutes / 60

    converted_mins = minutes - (hours * 60)
    converted_secs = @seconds - (minutes * 60)

    "#{padded(hours)}:#{padded(converted_mins)}:#{padded(converted_secs)}"
  end

  def padded(num)
    if num < 10
      "0#{num}"
    else
      num
    end
  end
end
