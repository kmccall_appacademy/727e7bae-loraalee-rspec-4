class Dictionary
  attr_accessor :entries, :keywords

  def initialize(entries = {})
    @entries = entries
    @keywords = []
  end

  def add(new_entry)
    if new_entry.is_a?(Hash)
      @entries.merge!(new_entry)
      @keywords << new_entry.keys[0]
    else
      @entries[new_entry] = nil
      @keywords << new_entry
    end
  end

  def include?(word)
    if word
      if @entries.key?(word)
        true
      else
        false
      end
    else
      nil
    end
  end

  def find(word)
    @entries.select { |k, v| k.match(word) }
  end

  def keywords
    @keywords.sort
  end

  def printable
    words_and_defs = []

    @entries.each do |word, definition|
      words_and_defs << "[#{word}] \"#{definition}\""
    end

    res = words_and_defs.sort.join("\n")
    "#{res}"
  end
end
