class Book
  attr_accessor :title

  def title
    exception_words = ["the", "and", "in", "of", "a", "an"]
    title_words = @title.split(" ")

    title_words.each_with_index do |word, index|
      if index != 0
        word.capitalize! unless exception_words.include?(word)
      else
        word.capitalize!
      end
    end

    title_words.join(" ")
  end
end
