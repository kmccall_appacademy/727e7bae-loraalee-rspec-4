class Temperature
  attr_accessor :temp_fahrenheit, :temp_celsius

  def initialize(opts = {})
    @temp_fahrenheit = opts[:f]
    @temp_celsius = opts[:c]
  end

  def in_fahrenheit
    if @temp_fahrenheit
      @temp_fahrenheit
    else
      @temp_celsius * 1.8 + 32
    end
  end

  def in_celsius
    if @temp_celsius
      @temp_celsius
    else
      ((@temp_fahrenheit - 32) / 1.8).round
    end
  end

  def self.from_celsius(temp)
    Temperature.new({:c => temp})
  end

  def self.from_fahrenheit(temp)
    Temperature.new(opts = {:f => temp})
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp_celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp_fahrenheit = temp
  end
end
